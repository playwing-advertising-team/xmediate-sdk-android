package com.xmediate.SampleApp;

import com.xmediate.base.ads.adsettings.XmAdSettings;
import com.xmediate.base.ads.adsettings.XmGender;
import com.xmediate.base.ads.adsettings.XmMaritalStatus;
import com.xmediate.base.ads.adsettings.XmSexualOrientation;

import java.util.HashMap;

/**
 * Created by X-Mediate on 30/09/17.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: X-Mediate ad setting
 */

@SuppressWarnings("WeakerAccess")
public class AdSettingsSampleGenerator {
    public XmAdSettings getAdSettings() {
        XmAdSettings adSettings = new XmAdSettings();
        // set location (longitude, latitude, and accuracy)
        adSettings.setLocation(10.0, 10.0, 1);

        // if testing mode
        adSettings.setTesting(true);
        // replace e411125c0629737f8a1747396e9a6d27 with device id printed in log
        adSettings.setTestDevices(new String[]{"e411125c0629737f8a1747396e9a6d27"});

        // if you want to set age
        adSettings.setAge(25);

        // date of birth
        adSettings.setDateOfBirth(10, 10, 1990);

        // if you want to set gender
        adSettings.setGender(XmGender.MALE);

        // if you want to set Martial status
        adSettings.setMartialStatus(XmMaritalStatus.UNMARRIED);

        // if you want to set sexualOrientation
        adSettings.setSexualOrientation(XmSexualOrientation.STRAIGHT);

        // if you want to set area code
        adSettings.setAreaCode("9980-999");

        // if you want to set Education
        adSettings.setEducation("B.Tech");

        // income
        adSettings.setIncome(100000);

        // language
        adSettings.setLanguage("English");

        //some keywords to define user interests or features or any other params
        adSettings.setKeyWords("cricket, football, and some other things");

        //Any other custom parameter
        //adSettings.setExtraParameter("param-key", "param-value");

        //if you want to pass a set of custom parameter
        HashMap<String, String> extraParams = new HashMap<>();
        extraParams.put("key1", "value1");
        extraParams.put("key2", "value2");
        extraParams.put("key3", "value3");
        extraParams.put("key4", "value4");
        adSettings.setExtraParameterSet(extraParams);

        return adSettings;
    }
}
