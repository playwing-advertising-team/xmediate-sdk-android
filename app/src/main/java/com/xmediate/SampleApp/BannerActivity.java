package com.xmediate.SampleApp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xmediate.base.ads.XmAdView;
import com.xmediate.base.ads.XmBannerAdListener;
import com.xmediate.base.ads.XmBannerType;
import com.xmediate.base.ads.XmErrorCode;
import com.xmediate.base.ads.adsettings.XmAdSettings;


 /*
 * Created by X-Mediate on 30/09/17.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: X-Mediate sdk Banner ad
 */

public class BannerActivity extends AppCompatActivity {

    private Activity context;
    private XmAdView mBannerAdView;
    private Button mLoadBannerButton;
    private Button mDestroyBannerButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        this.context = this;
        mLoadBannerButton = (Button) findViewById(R.id.btn_load_banner_ad);
        mDestroyBannerButton = (Button) findViewById(R.id.btn_destroy_banner_ad);

        mLoadBannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toggleEnableForLoadButton(false);
                        loadAd();
                    }
                });
            }
        });

        mDestroyBannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        destroyAd();
                        toggleEnableForLoadButton(true);
                    }
                });
            }
        });
    }

    private void loadAd() {

        mBannerAdView = (XmAdView) findViewById(R.id.xm_banner_ad_view);

        mBannerAdView.setAdListener(new XmBannerAdListener() {
            @Override
            public void onBannerLoaded(String tag) {
                toggleEnableForLoadButton(true);
                showToast("onBannerLoaded :: " + tag);
            }

            @Override
            public void onBannerFailedToLoad(String tag, XmErrorCode errorCode) {
                toggleEnableForLoadButton(true);
                showToast("onBannerFailedToLoad :: " + errorCode.toString() + " :: " + tag);
            }

            @Override
            public void onBannerExpanded(String tag) {
                toggleEnableForLoadButton(true);
                showToast("onBannerExpanded :: " + tag);
            }

            @Override
            public void onBannerCollapsed(String tag) {
                toggleEnableForLoadButton(true);
                showToast("onBannerCollapsed :: " + tag);
            }

            @Override
            public void onBannerClicked(String tag) {
                toggleEnableForLoadButton(true);
                showToast("onBannerClicked :: " + tag);
            }

            @Override
            public void onLeaveApplication(String tag) {
                toggleEnableForLoadButton(true);
                showToast("onLeaveApplication  :: " + tag);
            }
        });

        mBannerAdView.setXmBannerType(XmBannerType.BANNER);
        XmAdSettings adSettings = new AdSettingsSampleGenerator().getAdSettings();
        mBannerAdView.loadAd(adSettings);
    }

    private void destroyAd() {
        if (mBannerAdView != null) {
            mBannerAdView.destroy();
            mBannerAdView = null;
        }
    }

    private void toggleEnableForLoadButton(boolean isEnabled) {
        mLoadBannerButton.setEnabled(isEnabled);
        mDestroyBannerButton.setEnabled(isEnabled);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        destroyAd();
        super.onDestroy();
    }
}
