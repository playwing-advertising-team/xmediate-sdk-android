package com.xmediate.SampleApp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.xmediate.base.ads.XmErrorCode;
import com.xmediate.base.ads.XmFullScreenAdListener;
import com.xmediate.base.ads.XmFullscreenAd;
import com.xmediate.base.ads.XmFullscreenAdVideoPlayBackListener;
import com.xmediate.base.ads.adsettings.XmAdSettings;
import com.xmediate.base.ads.XmFullScreenAdType;

/**
 * Created by X-Mediate on 31/01/17.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: X-Mediate sdk Interstitial ad
 */

public class FullScreenActivity extends AppCompatActivity {
    private XmFullscreenAd xmFullscreenAd;
    private Button loadFullScreenButton;
    private Button showFullScreenButton;
    private Button destroyFullScreenButton;
    private Spinner spinner;
    private XmFullScreenAdType fsTYpe = XmFullScreenAdType.FULL_SCREEN_TYPE_DEFAULT;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        loadFullScreenButton = (Button) findViewById(R.id.btn_load_interstitial_ad);
        showFullScreenButton = (Button) findViewById(R.id.btn_show_interstitial_ad);
        destroyFullScreenButton = (Button) findViewById(R.id.btn_destroy_interstitial_ad);
        spinner = (Spinner) findViewById(R.id.spinner_fullscreen);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                switch (pos) {
                    case 0:
                        fsTYpe = XmFullScreenAdType.FULL_SCREEN_TYPE_DEFAULT;
                        break;
                    case 1:
                        fsTYpe = XmFullScreenAdType.FULL_SCREEN_TYPE_INTERSTITIAL;
                        break;
                    case 2:
                        fsTYpe = XmFullScreenAdType.FULL_SCREEN_TYPE_VIDEO;
                        break;
                    default:
                        fsTYpe = XmFullScreenAdType.FULL_SCREEN_TYPE_DEFAULT;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleEnableForLoadButton(false);
                loadFullScreenAds();
            }
        });

        showFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFullscreen();
            }
        });

        destroyFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destroyFullscreen();
                toggleEnableForLoadButton(true);
            }
        });
        initSpinner();
    }

    private void initSpinner() {
        String[] items = new String[]{"Default", "Interstitial", "Video"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
    }


    private void loadFullScreenAds() {
        if (xmFullscreenAd == null) {
            xmFullscreenAd = new XmFullscreenAd(this);
            xmFullscreenAd.setFullScreenAdListener(new XmFullScreenAdListener() {
                @Override
                public void onFullScreenAdLoaded(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("full screen ad loaded :: " + tag);
                }

                @Override
                public void onFullScreenAdFailedToLoad(String tag, XmErrorCode error) {
                    toggleEnableForLoadButton(true);
                    showToast("full screen ad failed to laod :: " + error.toString());
                }

                @Override
                public void onFullScreenAdShown(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onInterstitialShown :: " + tag);
                }

                @Override
                public void onFullScreenAdDismissed(String tag) {
                    showToast("full screen dismissed :: " + tag);

                }

                @Override
                public void onFullScreenAdClicked(String tag) {
                    showToast("full screen clicked :: " + tag);

                }

                @Override
                public void onFullScreenAdLeaveApplication(String tag) {
                    showToast("full screen will leave application :: " + tag);

                }

                @Override
                public void onFullScreenAdExpiring(String tag) {
                    showToast("full screen expiring :: " + tag);

                }
            });
            xmFullscreenAd.setVideoPlayBackListener(new XmFullscreenAdVideoPlayBackListener() {
                @Override
                public void onFullScreenVideoAdStartedPlaying(String s) {
                    showToast("FullScreenVideo video started");
                }

                @Override
                public void onFullScreenVideoAdFailedToPlay(String tag, XmErrorCode xmErrorCode) {
                    showToast("FullScreenVideo video failed to play with " + xmErrorCode.toString());

                }

                @Override
                public void onFullScreenVideoAdComplete(String s) {
                    showToast("FullScreenVideo video complete");
                }
            });
        }
        XmAdSettings adSettings = new AdSettingsSampleGenerator().getAdSettings();
        xmFullscreenAd.load(fsTYpe, adSettings);
    }

    private void showFullscreen() {
        if (xmFullscreenAd != null) {
            // Note: Interstitial ads take time to load,
            // Load the ad before use and at the time of showing check if ad is loaded and then show
            if (xmFullscreenAd.isReady()) {
                toggleEnableForLoadButton(false);
                xmFullscreenAd.show();
            } else {
                showToast("FullScreen is not loaded yet. Please click Load FullScreen button.");
            }
        } else {
            showToast("FullScreen object is null");
        }
    }

    private void destroyFullscreen() {
        if (xmFullscreenAd != null) {
            xmFullscreenAd.destroy();
            xmFullscreenAd = null;
        }
    }

    private void toggleEnableForLoadButton(boolean isEnabled) {
        loadFullScreenButton.setEnabled(isEnabled);
        showFullScreenButton.setEnabled(isEnabled);
        destroyFullScreenButton.setEnabled(isEnabled);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        destroyFullscreen();
        super.onDestroy();
    }
}
