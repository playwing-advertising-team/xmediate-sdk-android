package com.xmediate.SampleApp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xmediate.base.ads.XmErrorCode;
import com.xmediate.base.ads.XmRewardItem;
import com.xmediate.base.ads.XmRewardedVideoAd;
import com.xmediate.base.ads.XmRewardedVideoAdListener;

/**
 * Created by X-Mediate on 31/01/17.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: X-Mediate sdk rewarded Video ad
 */

public class RewardedVideoActivity extends AppCompatActivity {
    private XmRewardedVideoAd mXmRewardedVideoAd;
    private Button mLoadRewardedVideoButton;
    private Button mShowRewardedVideoButton;
    private Button mDestroyRewardedVideoButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewarded_video);

        mLoadRewardedVideoButton = (Button) findViewById(R.id.btn_load_rewarded_video_ad);
        mShowRewardedVideoButton = (Button) findViewById(R.id.btn_show_rewarded_video_ad);
        mDestroyRewardedVideoButton = (Button) findViewById(R.id.btn_destroy_rewarded_video_ad);

        mLoadRewardedVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleEnableForLoadButton(false);
                loadRewardedVideo();
            }
        });

        mShowRewardedVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRewardedVideo();
            }
        });

        mDestroyRewardedVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destroyRewardedVideo();
                toggleEnableForLoadButton(true);
            }
        });
    }

    private void loadRewardedVideo(){
        toggleEnableForLoadButton(false);
        if (mXmRewardedVideoAd == null) {

            showToast("Load in progress");
            mXmRewardedVideoAd = new XmRewardedVideoAd(this);
            mXmRewardedVideoAd.setRewardedVideoAdListener(new XmRewardedVideoAdListener() {
                @Override
                public void onRwdVideoLoaded(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoLoaded :: "+ tag);
                }

                @Override
                public void onRwdVideoFailedToLoad(String tag, XmErrorCode errorCode) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoFailedToLoad :: tag: "+ tag + " :: ErrorCode: "+errorCode.toString());
                }

                @Override
                public void onRwdVideoOpened(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoOpened :: "+ tag);
                }

                @Override
                public void onRwdVideoStartedPlaying(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoStartedPlaying :: "+ tag);
                }

                @Override
                public void onRwdVideoFailedToPlay(String tag, XmErrorCode errorCode) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoFailedToPlay :: "+ tag);
                }

                @Override
                public void onRwdVideoClicked(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoClicked :: "+ tag);
                }

                @Override
                public void onRwdVideoComplete(final String tag, XmRewardItem rewardItem) {
                    toggleEnableForLoadButton(true);
                    if(rewardItem != null) {
                        showToast("onRwdVideoComplete :: " + tag + "::" + rewardItem.getType() + " - " + rewardItem.getAmount());
                    } else {
                        showToast("onRwdVideoComplete :: " + tag);
                    }
                }

                @Override
                public void onRwdVideoClosed(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoClosed :: "+ tag);
                }

                @Override
                public void onRwdVideoLeftApplication(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoLeftApplication :: "+ tag);
                }

                @Override
                public void onRwdVideoExpiring(String tag) {
                    toggleEnableForLoadButton(true);
                    showToast("onRwdVideoExpiring :: "+ tag);
                }
            });
        }

        mXmRewardedVideoAd.load(new AdSettingsSampleGenerator().getAdSettings());
    }

    private void showRewardedVideo(){
        if(mXmRewardedVideoAd != null){
            // Note:  video ads take time to load,
            // Load the ad before use and at the time of showing check if ad is loaded and then show
            if(mXmRewardedVideoAd.isReady()) {
                toggleEnableForLoadButton(false);
                mXmRewardedVideoAd.show();
            } else {
                showToast("Rewarded Video is not loaded yet. Please click Rewarded Load Video button.");
            }
        } else {
            showToast("XM IS NULL Rewarded Video is not loaded yet. Please click Rewarded Load Video button.");
        }
    }

    private void destroyRewardedVideo(){
        if(mXmRewardedVideoAd != null){

            mXmRewardedVideoAd.invalidate();

            showToast("Invalidate completed");

            mXmRewardedVideoAd.destroy();

            showToast("Destroy completed");

            mXmRewardedVideoAd = null;


        }

        showToast("If ended, null attributed");
    }

    private void toggleEnableForLoadButton(boolean isEnabled) {
        mLoadRewardedVideoButton.setEnabled(isEnabled);
        mShowRewardedVideoButton.setEnabled(isEnabled);
        mDestroyRewardedVideoButton.setEnabled(isEnabled);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        destroyRewardedVideo();
        super.onDestroy();
    }
}
