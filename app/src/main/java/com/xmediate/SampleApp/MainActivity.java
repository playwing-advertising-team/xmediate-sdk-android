package com.xmediate.SampleApp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.xmediate.base.XMediate;

/**
 * Updated by X-Mediate on 23/05/18.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: App MainActivity
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    Context context;
    Button btnLoadBanner;
    Button btnLoadFullScreen;
    Button btnLoadRewardedVideo;

    private boolean[] flags = new boolean[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showGDPRDialog();

        context = this;

        btnLoadBanner = (Button) findViewById(R.id.btn_banner_ad);
        btnLoadBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BannerActivity.class);
                startActivity(intent);
            }
        });

        btnLoadFullScreen = (Button) findViewById(R.id.btn_interstitial_ad);
        btnLoadFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FullScreenActivity.class);
                startActivity(intent);
            }
        });

        btnLoadRewardedVideo = (Button) findViewById(R.id.btn_rewarded_video_ad);
        btnLoadRewardedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RewardedVideoActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showGDPRDialog() {

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.xmediate_logo)
                .setTitle("GDPR Consent")
                .setMultiChoiceItems(new String[]{"Is GDPR Country", "Accept GDPR"}, flags,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                flags[which] = isChecked;
                            }
                        })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e(TAG, "---- " + flags[0] + " " + flags[1] + " ----");
                        XMediate.updateGDPRSettings(getApplicationContext(), flags[0], flags[1]);
                    }
                }).create()
                .show();
    }
}
