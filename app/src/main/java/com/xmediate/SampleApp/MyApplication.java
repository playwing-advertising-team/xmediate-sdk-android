package com.xmediate.SampleApp;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


import com.xmediate.base.XMediate;


/**
 * Created by X-Mediate on 31/01/17.
 * Copyright © 2017 X-Mediate. All rights reserved.
 * Use: X-Mediate sdk initialization
 */

public class MyApplication extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        XMediate.setBannerRefreshTime(30);
        /*
         * @param application - application parameter (pass this)
         * @param pubId - publisher Id
         * @param appId - application Id
         */

        //X-Mediate initializing
        XMediate.init(this, "YOUR-PUBLISHER-ID", "YOUR-APPICATION-ID");
    }
}
